package main
import (
    "bufio"
    "flag"
    "fmt"
    "os"
    "strings"
    "github.com/gojp/kana"
)

var phrase,output string
func romaji2hiragana(phrase string) string {
            fmt.Println("romaji to hiragana")
            output := kana.RomajiToHiragana(phrase)
            fmt.Println(output)
            return output
}    
func romaji2katakana(phrase string) string {
    fmt.Println("romaji to katakana")
    output := kana.RomajiToKatakana(phrase)
    fmt.Println(output)
    return output
}
func kana2romaji(phrase string) string {
        fmt.Println("kana to romaji")
        output := kana.KanaToRomaji(phrase)
        fmt.Println(output)
        return output
}
func katakana2hiragana(phrase string) string {
    fmt.Println("katakana to hiragana")
    output := strings.Map(processkatakana, phrase)
    fmt.Printf ("\n%s\n", output)
    return output
}
func hiragana2katakana(phrase string) string {
    fmt.Println("hiragana to katakana")
    output := strings.Map(processhiragana, phrase)
    fmt.Printf ("\n%s\n", output)
    return output
}
func processkatakana(char rune) rune {
    if (char >= 'ァ' && char <= 'ヶ') || (char >= 'ヽ' && char <= 'ヾ') {
        return char - 0x60
    }
    return char
}
func processhiragana(char rune) rune {
    if (char >= 'ぁ' && char <= 'ゖ') || (char >= 'ゝ' && char <= 'ゞ') {
        return char + 0x60
    }
    return char
}
func verbose(phrase string) (bool, bool)  {
    iskana := kana.IsKana(phrase)
    isromaji := kana.IsLatin(phrase)
    fmt.Println("The phrase is")
    fmt.Println("[KANA]: ")
    fmt.Println(iskana)
    fmt.Println("[ROMAJI]: ")
    fmt.Println(isromaji)
    return iskana,isromaji
}
func getinput() string{
    kbdreader := bufio.NewReader(os.Stdin)
    fmt.Println("Conversion")
    fmt.Println("Enter a word or phrase to convert:")
    input, _ := kbdreader.ReadString('\n')
    phrase := strings.TrimSuffix(input, "\n")
    return phrase
}
func main() {
    eromkan_version := "1.0"
    verbptr := flag.Bool("-verbose", false, "Verbose output")
    versionptr := flag.Bool("v", false, "Show version")
    r2hptr := flag.Bool("r2h", false, "Convert romaji to hiragana")
    r2kptr := flag.Bool("r2k", false, "Convert romaji to katakana")
    k2rptr := flag.Bool("k2r", false, "Convert kana to romaji")
    k2hptr := flag.Bool("k2h", false, "Convert katakana to hiragana (legacy mode only)")
    h2kptr := flag.Bool("h2k", false, "Convert hiragana to katakana (legacy mode only)")
    legbehptr := flag.Bool("l", false, "Use application in legacy mode")
    text2conv := flag.String("t2c", "foo", "Text to convert (works in usual (non-legacy) mode only)")
    flag.Parse()
    if *versionptr == true {
        fmt.Println("eromkan version", eromkan_version)
    }
    if *legbehptr == true {
        if *r2hptr == true {
            phrase := getinput()        
            romaji2hiragana(phrase)
        }
        if *r2kptr == true {
            phrase := getinput()
            romaji2katakana(phrase)
        }
        if *k2rptr == true {
            phrase := getinput()
            kana2romaji(phrase)
        }
        if *k2hptr == true {
            phrase := getinput()
            katakana2hiragana(phrase)
        }
        if *h2kptr == true {
            phrase := getinput()
            hiragana2katakana(phrase)
        }
        fmt.Println(output)
        if *verbptr == true {
            phrase := getinput()
            verbose(phrase)
        }
    } 
    if *text2conv != "foo" {
        if *r2hptr == true {                                        
		romaji2hiragana(*text2conv)
        }
        if *r2kptr == true {
		romaji2katakana(*text2conv)
        }
        if *k2rptr == true {
		kana2romaji(*text2conv)
        }
        fmt.Println(output)
        if *verbptr == true {
		verbose(*text2conv)
        }
    }
}
